

class ConnectionManager:
    def __init__(self, connection):
        self.conn = connection

    def num_clients(self):
        return len(self.conn.client_list())

    def get_first_client_name(self):
        return self.conn.client_list()[0]['name']

    def push_to_queue(self, queue_name: str, message: str):
        self.conn.rpush(queue_name, message)

    def pop_from_queue(self, queue_name: str, timeout: int = 1):
        return self.conn.blpop(queue_name, timeout)
