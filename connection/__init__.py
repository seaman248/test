import redis
import os

REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', 1000)

pool = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=0)

conn = redis.Redis(connection_pool=pool)

main_queue = 'main'

errors_queue = 'errors'