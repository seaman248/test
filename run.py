from app import App
from sys import argv


if __name__ == '__main__':
    try:
        if argv[1] == 'getErrors':
            App().get_errors()
    except IndexError:
        App().run()
