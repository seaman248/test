from marshmallow import Schema, fields, post_load

MODE_PRODUCER = 0
MODE_WORKER = 1


class Message(object):
    def __init__(self, content):
        self.content = content

    def __repr__(self):
        return '<Message(content={self.content})>'.format(
            self=self)


class MessageSchema(Schema):
    content = fields.Str()

    @post_load
    def make_message(self, data):
        return Message(**data)
