from app import constants as c
from app.loop import ApplicationMainLoop
from connection import conn, errors_queue

from faker import Faker
from app.errors import EmptyMessageError

import logging

logging.basicConfig(level='INFO')


class App:

    def __init__(self):
        self.name = Faker().first_name()

        logging.info('Application has been started')

        conn.client_setname(self.name)

        self.loop = ApplicationMainLoop(self.name)

    def get_errors(self):
        while True:
            error = self.loop.conn_manager.pop_from_queue(errors_queue)
            if not error:
                print('Done')
                return
            print(error[1].decode('UTF-8'))

    def run(self):
        while True:
            try:
                self.loop.do()
            except EmptyMessageError as e:
                self.loop.conn_manager.push_to_queue(errors_queue, str(e))
