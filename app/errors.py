from datetime import datetime as dt
import logging


class EmptyMessageError(Exception):
    def __init__(self, message: str):
        self.message = message
        super().__init__(self.message)
        logging.error(self.message)

    def __repr__(self):
        return str(dt.now()) + ' / ' + self.message

    def __str__(self):
        return str(dt.now()) + ' / ' + self.message
