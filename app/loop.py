from app import constants as c
from connection import conn
from modes import AppMode
from modes.producer import Producer
from modes.worker import Worker
from connection.connection_manager import ConnectionManager
import logging


class ApplicationMainLoop:

    MODE = {
        c.MODE_WORKER: Worker,
        c.MODE_PRODUCER: Producer
    }

    instance_mode: AppMode

    def __init__(self, instance_name: str):
        self.instance_name = instance_name
        self.switch_mode(c.MODE_WORKER)
        self.conn_manager = ConnectionManager(conn)

    def switch_mode(self, mode: int):
        self.instance_mode = self.MODE[mode]
        logging.info(f'Set mode as {self.instance_mode}')

    def do__check_whether_to_be_producer_or_worker(self):
        if self.instance_mode.get_mode() == c.MODE_PRODUCER:
            return

        if self.conn_manager.get_first_client_name() == self.instance_name:
            self.switch_mode(c.MODE_PRODUCER)

    def do__work(self):
        self.instance_mode.do_work()

    def do(self):
        handle_functions = \
            [func for func in dir(self) if func.startswith('do__')]
        for func in handle_functions:
            getattr(self, func)()
