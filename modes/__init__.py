from connection import conn


class AppMode:

    MODE: int

    @classmethod
    def get_mode(cls):
        return cls.MODE

    def __init__(self, conn):
        self.conn = conn

    def _do_work(self):
        raise NotImplementedError

    @classmethod
    def do_work(cls):
        cls(conn=conn)._do_work()
