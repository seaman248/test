from uuid import uuid1

from modes import AppMode

from connection import main_queue
from app.constants import MODE_PRODUCER, MessageSchema, Message
from connection.connection_manager import ConnectionManager
from random import randrange
import logging

import time


class Producer(AppMode):

    MODE = MODE_PRODUCER

    @staticmethod
    def generate_content() -> str:
        if randrange(20) == 0:
            return ''

        content = str(uuid1())

        return content

    def gen_message(self):
        content: str = self.generate_content()

        message: Message = Message(content=content)

        return MessageSchema().dumps(message)[0]

    def check_number_of_workers(self):
        return len(self.conn.client_list())

    def _do_work(self):
        conn_manager = ConnectionManager(self.conn)

        if conn_manager.num_clients() < 2:
            sec_to_waiting_for = 3
            logging.info(
                f'Workers not found. Waiting for {sec_to_waiting_for} sec.')
            time.sleep(sec_to_waiting_for)
            return

        message = self.gen_message()
        logging.info(f'Send message to worker {message}')
        conn_manager.push_to_queue(main_queue, message)

        time.sleep(0.5)

