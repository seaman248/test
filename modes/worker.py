from modes import AppMode
from app.constants import MODE_WORKER, MessageSchema, Message
import time
from connection import main_queue
from connection.connection_manager import ConnectionManager
from app.errors import EmptyMessageError
import logging


class Worker(AppMode):

    MODE = MODE_WORKER

    def _do_work(self):
        conn_manager = ConnectionManager(self.conn)

        data = conn_manager.pop_from_queue(main_queue, timeout=1)

        if not data:
            return

        message: Message = MessageSchema()\
            .loads(data[1]).data

        if not message.content:
            raise EmptyMessageError('Message is empty')

        logging.info(f'Message received: {message.content}')

        time.sleep(1)
